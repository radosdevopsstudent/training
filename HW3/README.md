**Homework №3 from 19.01.21**

# The task 1.

**Users and groups.**

1. Создайте группу sales с GID 4000 и пользователей bob, alice, evec основной группой sales.

 To create a group "sales" with GID 4000:

   >sudo groupadd -g 4000 sales
   
   ![1](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/1.PNG)

 Then to add users bob, alice and eve:

   >sudo useradd -m -G sales bob
   
   >sudo useradd -m -G sales alice
   
   >sudo useradd -m -G sales eve
   
   ![2](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/2.PNG)
   
2. Измените пользователям пароли.
   
 After that, I change passwods for users bob, alice and eve:
 
   >sudo passwd bob
  
   >sudo passwd alice
  
   >sudo passwd eve
   
   ![3](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/3.PNG)
   
3. Все новые аккаунты должны обязательно менять свои пароли каждый 30 дней.

 We set a time interval so that users change their password every 30 days:
 
   >sudo chage -E $(date -d +30days +%Y-%m-%d) bob
   
   >sudo chage -E $(date -d +30days +%Y-%m-%d) alice
   
   >sudo chage -E $(date -d +30days +%Y-%m-%d) eve
   
   ![4](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/4.PNG)
   
 We did this):
 
   ![5](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/5.PNG)
   
4. Новые аккаунты группы sales должны истечь по окончанию 90 дней срока, а bob должен изменять его пароль каждые 15 дней.

 First, we will know the date when 90 days will expire from today:
   
   >date -d "+90 days" +%F
   
   ![6](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/6.PNG)
 
 Set the time when accounts will expire:
   
   >sudo chage -E 2021-04-21 bob
   
   >sudo chage -E 2021-04-21 alice
   
   >sudo chage -E 2021-04-21 eve
   
   ![7](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/7.PNG)
   
5. Заставьте пользователей сменить пароль после первого логина.

   >sudo passwd -e bob
   
   >sudo passwd -e alice
   
   >sudo passwd -e eve
   
   ![8](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/8.PNG)
   
 As we can see, everything is working:
 
   ![9](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/9.PNG)
 
6. bob должен изменять его пароль каждые 15 дней.
 
   >sudo chage -M 15 bob
   
 Now he have to change his password every 15 days.
 
   ![10](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/10.PNG)
 
# The task 2.

1. У вас должна быть директория /home/students, где эти три пользователя могут работать совместно с файлами.

 To create a directiry "students" at directiry "home":

   >mkdir students
   
   ![11](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/11.PNG)
   
2. Создайте трёх пользователей glen, antony, lesly.

   >sudo useradd glen
   
   >sudo useradd antony
   
   >sudo useradd lesly
   
 Check our users with help command "cat /etc/passwd":
 
   ![12](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/12.PNG)
   
3. Должен быть возможен только пользовательский и групповой доступ, создание и удаление файлов в /home/students.
 
 For this task I creat a group "students":
 
   >sudo groupadd students
   
 Then I add all users glen, antony and lesly in this group:
 
   >sudo usermod -a -G students glen
   
   >sudo usermod -a -G students antony
   
   >sudo usermod -a -G students lesly

 Everybody in group "students":
 
   ![13](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/13.PNG)
   
 Then I added the students group to the list of students interacting with the folder:
 
   >sudo chown :students /home/radosteva/students

 Gave all permision to user and groups:
 
   >chmod ug+rwx /home/radosteva/students
   
 And  I take away the permision from other users:
 
   >chmod o-rwx /home/radosteva/students
   
 As we can see, work was done)
 
   ![14](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/14.PNG)
   
4. Файлы, созданные в этой директории, должны автоматически присваиваться группе студентов students.

   >sudo chmod g+s /home/students
   
# The task 3. 

1. Создайте общую директорию /shares/cases.

   >sudo mkdir -p /shares/cases
   
   ![15](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/15.PNG)
   
2. Создайте группу bakerstreet с пользователями holmes, watson. Group has nomber 4002.
 
 To create group backerstreet:
   
   >sudo groupadd backerstreet
   
 To create users holmes and watson in group backerstreet:
 
   >sudo useradd holmse -g backerstreet

   >sudo useradd watson -g backerstreet
   
   ![16](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/16.PNG)
   
3. Создайте группу scotlandyard с пользователями lestrade, gregson, jones. Group has nomber 4003.
   
 To create group scotlandyard:
   
   >sudo groupadd scotlandyard
   
 To create users lestrade, gregson and jones in group scotlandyard:
   
   >sudo useradd lestrade -g scotlandyard
   
   >sudo useradd gregson -g scotlandyard
   
   >sudo useradd jones -g scotlandyard
   
   ![17](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/17.PNG)
   
4. Задайте всем пользователям безопасные пароли.
   
   >sudo passwd holmes (DlrBr7)
   
   >sudo passwd watson (T88QMh)
   
   >sudo passwd lestrade (vPNFXz)
   
   >sudo passwd gregson (71bYKc)
   
   >sudo passwd jones (skthtP)
   
5. От суперпользователя создайте папку /shares/cases и создайте внутри 2 файла murders.txt и moriarty.txt

   >sudo touch murders.txt
   
   >sudo touch moriarty.txt
   
6. В задании говорится, что группы  backerstreet и scotlandyard должны иметь права на чтение и запись в директории /shares/cases. При этом у других пользователей не должно быть никаких разрешений.

 First, I set access to the folder /shares/cases:

   >sudo chmod g-x /shares/cases
   
   >sudo chmod o-rx /shares/cases
   
   ![18](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/18.PNG)
   
7. Затем, директория и всё её содержимое должно принадлежать группе bakerstreet, при этом файлы должны обновляться для чтения и записи для владельца и группы (bakerstreet):

 I made group backerstreet the owner:
 
   >sudo chown :backerstreet /shares/cases
   
8. При этом файлы должны обновляться для чтения и записи для владельца и группы (bakerstreet).
   
 I added the group scotlandyard and backerstreet in the list of default interacting groups:
   
   >sudo setfacl -R -m g:backerstreet:rwx /shares/cases
   
   >sudo setfacl -R -m g:scotlandyard:rwx /shares/cases

   ![20](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/20.PNG)
   
9. Jones может только читать документы в директории /shares/cases:

   >sudo setfacl -R -m u:jones:rx /shares/cases
   
   ![21](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/21.PNG)

   ![22](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/22.PNG)

10. Now check other users from other groups:
 
 holmes and watson can check directory /shares/cases, create and edit files:
 
   ![23](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/23.PNG)
   
   ![24](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/24.PNG)
   
 Group backerstreet can do do all the necessary actions in directory /shares/cases.
 
 From user lestrade from group scotlandyard was done all the necessary actions in directory /shares/cases. (except jones, of course. We checked him before)  
 
   ![25](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW3/25.PNG)
   
 So, I can do conclude that all authorized users from group scotlandyard can do all what they need in directory /shares/cases.
   
   
