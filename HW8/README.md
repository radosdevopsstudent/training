**Homework №8 from 03.02.21**

# The tasks for 15.

**Подключить репозиторий docker community edition. Установить docker-ce версии 19.03.14. Убедиться, что установлена нужная версия.**

 First, will connect Docker to our yum.
 
   >yum install yum-utils device-mapper-persistent-data lvm2
 
   >yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
 
 Then we look at which version of Docker is available to us.
 
   >yum list docker-ce --showduplicates | sort -r
 
 After that, we will install the needs virsion of Docker-ce.
 
   >sudo yum install docker-ce-19.03.14
 
 And start work our Docker-ce.
 
   >systemctl start docker
 
 Check :)
 
   >sudo docker versin
 
![1](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW8/1.PNG)

**Обновить docker-ce до последней версии.**

   >sudo yum update docker-ce
   
![2](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW8/2.PNG)

**Вывести список последних операций yum.**

   >history | grep yum
   
![3](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW8/3.PNG)

**Вывести полную информацию об установленном ранее пакете**

 We look at our last installed packages and then look all information about last package.

   >sudo rpm -qa | grep docker
   
   >sudo rpm -qi docker-ce-rootless-extras-20.10.3-3.el7.x86_64
   
![4](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW8/4.PNG)
 
**Удалить docker-ce**

   >sudo yum remove -y docker-ce
   
   >sudo rm -rd /var/lib/docker
   
# The tasks for 16.

**Переместить mlocate.db в новое место.**

 The first thing I saw was that I didn't have the "locate" command in my virtual machine, so I installed it.
 
 >sudo yum install mlocate
 
 >updatedb
 
 For task.
 
 >sudo mv /var/lib/mlocate/mlocate.db /home/radosteva
 
![5](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW8/5.PNG)
 
 **Создать новый файл file_task16.txt с любым содержанием и добавить информацию о нём в новый mlocate.db**
 
   >vi file_task16.txt
   
   >updatedb
   
   >locate file_task16.txt
   
![6](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW8/6.PNG)

**Найти файл file_task16.txt через locate и вывести его содержимое на экран (без явного указания полного пути к файлу).**

   >locate file_task16.txt | cat file_task16.txt
   
![7](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW8/7.PNG)

**Создать хардлинк на file_task16.txt, назвать его file_task16_hard.txt**

   >ln file_task16.txt file_task16_hard.txt
   
![8](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW8/8.PNG)

**Внести любые изменения в file_task16.txt. Удалить file_task16.txt. Вывести на экран file_task16_hard.txt, убедиться, что в нём отражены изменения.**

   >vi file_task16.txt
   
   >rm -f file_task16.txt
   
   >cat file_task16_hard.txt
   
![9](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW8/9.PNG)

# The task with "*"

**Создать именованный пайп pipe01**

   >mkfifo pipe01
   
**В первом терминале запустить считывание с pipe01 (любым способом, можно перечислить несколько)**

   >cat pipe01
   
**Создать софтлинк на пайп, назвать его pipe01-s.**

   >ln -s pipe01 pipe01-s
   
![10](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW8/10.PNG)

**Во втором терминале отправить в pipe01-s данные (любым способом, можно перечислить несколько)**

   >echo Hello > pipe01-s
   
**Убедиться, что данные были считаны первым терминалом**

![11](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW8/11.PNG)
