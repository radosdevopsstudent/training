**Homework №7 from 02.02.21**

**Bash**

# The task 1.

Find a sum of all running process' PIDs.

   ```
   #!/bin/bash -x
   sum=0
   for i in $( ps -e | awk '{print $1}' | grep -Eo '[0-9]{1,9}' )
   do
      sum=$(( $sum+$i ))
   done
   echo $sum
   
   ```
![1](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW7/1.PNG)

# The task 2.

A lucky number is one whose individual digits add up to 7, in successive additions. For example, 62431 is a lucky number (6 + 2 + 4 + 3 + 1 = 16, 1 + 6 = 7). Find all the lucky numbers between 1000 and 10000.

   ```
   
   #!/bin/bash -x
   for i in $(seq 1000 10000)
   do
   sum_1=0
   sum_2=0
   num=$i
   while [ $i -gt 0 ]; do
       ((sum_1 += i % 10))
       ((i /=10))
   done
   if [ ${#sum_1} -gt 1 ]
   then
        while [ $sum_1 -gt 0 ]; do
          ((sum_2 += sum_1 % 10))
          ((sum_1 /=10))
       done
       if [ $sum_2 -eq 7 ]
           then
           echo $num
        fi
   elif [ $sum_1 -eq 7 ]
   then
       echo $num
   fi
   done

   ```

# The task 3.

Write a script that takes a list of words (or even phrases) as an arguments. Script should ask a user to write something to stdin until user won't provide one of argument phrases.

   ```
   #!/bin/bash -x
   echo "My lovely colors is red, white and green"
   read i
   for w in $@
   do
   if [ $i == $w ]
   then
   echo "Thats ridht. Goodbye"
   exit 0
   fi
   done
   echo "Try again"
   
   ```
![2](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW7/2.PNG)

# The task 4.

As bash doesn't have any syntax standardisation a lot of bash users develop scripts that make further readers very unhappy. Also, these guys often over engineers such scripts. This is an example of this script. Please analyse a script and try to make it as readable and functional as possible from your sense of beauty.

   ```
   
   #!/bin/bash
   export SUM=0
   #сумма равна 0.
   for f in $(find src -name "*.java")
   #аргумент f будет равен количеству (найти в папке src все файлы, у которых в конце имени .java)
   do
   #делаем
      export SUM=$(($SUM + $(wc -l $f | awk '{ print $1 }')))
   #сумма SUM=равен количеству ( 0 + (посчитать все файлы и отсортировать по номеру подсчета)
   done
   #готово
   echo $SUM
   #эхаем результат

   ```

# The task 5.

1.1. As a first step, you should study a Shell Variables section of man bash, enable an unlimited history size and time stamping of command execution.

 In file .bashrc I added next:

   >export HISTTIMEFORMAT='%F %T '
   
   >export HISTSIZE=
   
   >export HISTFILESIZE=
   
   >reboot
   
![3](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW7/3.PNG)

**RegExp**

# The task 1.

Write a sed one-liner that will show stack traces lines.

 1.1. Create a file with with available data:
 
   >vi hw-7.2.1.txt
   
 1.2. Write regular regular expression:
 
   >sed -E 's/^\sat/You have a problem with entity/gm;t;d'
   
   >sed -E 's/\(/ You can find more info about it in file/gm;t;d'
   
   >sed -E 's/\:/ at line /gm;t;d'
   
   >sed -E 's/\)/ This file is written on java./gm;t;d'
   
 1.3. Then I added all this commands at 1:

   sed -E 's/^\sat/You have a problem with entity/;s/\(/\.\ You can find more info about it in file\ /;s/\:/\ at\ line\ /;s/\)/\.\ This file is written on java./gm;t;d'
   
![4](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW7/4.PNG)

# The task 2.

Write a RegEx that validates entries under `/etc/passwd`.

^([a-zA-Z0-9-_\+\.]+)\:(x)\:(\d+)\:(\d+)\:(.*)\:\/(\w*)\/(\w+)

![5](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW7/5.PNG)
