**Homework 2 from 14.01.21**
***
   At first, I downloaded the file with help command "curl":

 >curl http://www.almhuette-raith.at/apache-log/access.log > text.txt
***
**AWK**
# The task 1.

**What is the most frequent browser (user agent)?**

1.  I found line with user agent:
   >cat text.txt | awk -F\" '{print $6}'

2. Then I wrote script for fulfill 2 conditions of task. First, what is the most popular browser? Second, what is the most popular user agent? Run the script **awk -f myscript.awk text.txt**

    ```
    BEGIN { FS="\""; x=0; y=0; h=0 }
    {
    if ($6 ~ "Chrome") {x=x+1}
    if ($6 ~ "Firefox") {y=y+1}
    if ($6 ~ "Safari") {h=h+1}
    }
    END {
    {print "Chrome -" x}
    {print "firefox -" y}
    {print "Safari -" h}
    }
  
    BEGIN { FS="\""; q=0; w=0; e=0}
    {
    if ($6 ~ "Linux") {q=q+1}
    if ($6 ~ "Windows") {w=w+1}
    if ($6 ~ "iPhone") {e=e+1}
    }
    END {
    {print "Linux -" q}
    {print "Windows -" w}
    {print "iPhone -" e}
    }

 3.  As we can see:

![1](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW2/1.PNG)

   The most popular browser - Safari

   The most popular agent - Windows

# The task 2.

**Show number of requests per month for ip 193.106.31.130 (for example: Sep 2016 - 100500 reqs, Oct 2016 - 0 reqs, Nov 2016 - 2 reqs...)**

1. I found line with ip and date:

>cat text.txt | awk '{print $1 = "193.106.31.130",$2,$3,$4}'

2. Then, I wrote a script that helps give us the answer "How many requests was done from ip 193.106.31.130?". Run the script **awk -f myscript2.awk text.txt**

    ```
    BEGIN { FS="[[:space:]+]"; q=0; w=0; e=0; r=0; t=0; y=0; u=0; i=0; o=0; p=0; a=0;s=0; d=0 }
    {
    if ( $1 == "193.106.31.130" )
    {if ( $4 ~ "Jan/2021"){q=q+1}
    else if ( $4 ~ "Dec/2020") {w=w+1}
    else if ( $4 ~ "Nov/2020") {e=e+1}
    else if ( $4 ~ "Oct/2020") {r=r+1}
    else if ( $4 ~ "Sept/2020") {t=t+1}
    else if ( $4 ~ "Aug/2020") {y=y+1}
    else if ( $4 ~ "July/2020") {u=u+1}
    else if ( $4 ~ "June/2020") {i=i+1}
    else if ( $4 ~ "May/2020") {o=o+1}
    else if ( $4 ~ "Apr/2020") {p=p+1}
    else if ( $4 ~ "Mar/2020") {a=a+1}
    else if ( $4 ~ "Feb/2020") {s=s+1}
    else if ( $4 ~ "Jan/2020") {d=d+1}
    }}
    END {
    {print "Jan/2021 -" q}
    {print "Dec/2020 -" w}
    {print "Nov/2020 -" e}
    {print "Oct/2020 -" r}
    {print "Sept/2020 -" t}
    {print "Aug/2020 -" y}
    {print "July/2020 -" u}
    {print "June/2020 -" i}
    {print "May/2020 -" o}
    {print "Apr/2020 -" p}
    {print "Mar/2020 -" a}
    {print "Feb/2020 -" s}
    {print "Jan/2020 -" d}
    }

3. As we can see, the most part of requests was done in 1 month 2021 and 12 month 2020: 

![2](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW2/2.PNG)

# The task 3.

**Show total amount of data which server has provided for each unique ip (i.e. 100500 bytes for 1.2.3.4; 9001 bytes for 5.4.3.2 and so on)**

1. First, I transferred and sorted all IP and data into a new file: 

   >head text.txt | awk -F\  '{print $1,$10}' > databytes.txt

   >head databytes.txt | sort -r

2. We need to show total amount of data which server has provided for each unique ip. For this task I wrote next script. Run the script **awk -f myscript3.awk databytes.txt**:
    ```
    BEGIN {FS=" ";ipb=$2;ip=$1}
    {
    if($1==ip){ipb+=$2}
    else{print "ip: " ip " " ipb " bytes"; ip=$1;ipb=$2}
    }
3. We see, that we have need us information.

 ![3](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW2/3.PNG)
  

  **SED**

# The task 1

**Change all user agents to "lynx"**

1. We need to change all user agents to "lynx". For this I used command "sed" and wrote regular expression.

>sed "s/[(][^)]*[)]/\(lynx\)/g" text.txt > text_new.txt

![4](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW2/4.PNG)

# The task 2

**Masquerade all ip addresses. For example, 1.2.3.4 becomes "ip1", 3.4.5.6 becomse "ip2" and so on. Rewrite file.**

1. We need to masquerade all ip addresses. For this task I wrote next script and run it with command **bash sh.sh**
```
#!/bin/bash +x
i=0
for ip in $(awk '{print $1}' text.txt | sort | uniq)
do
   i=$((i+1))
   new_ip=ip${i}
   sed -i "s/$ip/$new_ip/g" text.txt
done
```
2. We see, that we done, what we need.

![5](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW2/6.PNG)

# Extra (*)

**Show list of unique ips, who made more then 50 requests to the same url within 10 minutes (for example too many requests to "/")**

1. All, what I can say about this task...

![6](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW2/unnamed.jpg)
