# Exam №1.

**Установить, настроить и запустить Hadoop Сore в минимальной конфигурации. Для этого потребуется подготовить 2 виртуальные машины: VM1 -headnode; VM2 -worker. Понимание принципов работы Hadoopи его компонентов для успешной сдачи задания не требуется.**

***If the task is intended for both virtual machines, then this description is duplicated for both virtual machines.***

**1. Установить CentOSна 2 виртуальные машины:

- VM1: 2CPU, 2-4G памяти, системный диск на 15-20G и дополнительные 2 диска по 5G

- VM2: 2CPU, 2-4G памяти, системный диск на 15-20G и дополнительные 2 диска по 5G**

VM1 - headnode :

![1](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/1.PNG)

VM2 - worker:

![2](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/2.PNG)

**2. При установке CentOS создать дополнительного пользователя exam и настроить для него использование sudo без пароля. Все последующие действия необходимо выполнять от этого пользователя, если не указано иное.**

For this task I did not checked the box next to the password request.

![8](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/8.PNG)

Then, for verification, I performed the action when I need to use the root password. I tryed turned on dhclient. At first I didn’t see my ip to connect, but then, after turned on dhclient, I saw it. There was no requirement for a root password.

![10](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/10.PNG)

**3. Установить OpenJDK8 из репозитория CentOS.**

Whaching needig version:

   >sudo yum search jdk
   
Then install:

   >sudo yum install -y java-1.8.0-openjdk-src.x86_64
   
![11](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/11.PNG)

**4. Скачать архив c Hadoop версии 3.1.2 (https://hadoop.apache.org/release/3.1.2.html).**

   >sudo wget https://archive.apache.org/dist/hadoop/common/hadoop-3.1.2/hadoop-3.1.2.tar.gz
   
![12](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/12.PNG)

**5. Распаковать содержимое архива в cd /opt/hadoop-3.1.2/**

   >sudo tar -C /opt/ -xvf hadoop-3.1.2.tar.gz
   
![13](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/13.PNG)

**6. Сделать симлинк /usr/local/hadoop/current/ на директорию /opt/hadoop-3.1.2/**

First, create directorys hadoop and current, because I don't have it.

   >sudo mkdir -p /usr/local/hadoop
   
Now symlink:

   >sudo ln -s /opt/hadoop-3.1.2/ /usr/local/hadoop/current
   
![14](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/14.PNG)

**7. Создать пользователей hadoop, yarn и hdfs,а также группу hadoop, в которую необходимо добавить всех этих пользователей**

First to create group "hadoop":

   >sudo groupadd hadoop
   
![15](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/15.PNG)
   
Then users:

   >sudo useradd -m -g hadoop hadoop
   
   >sudo useradd -m -g hadoop yarn
   
   >sudo useradd -m -g hadoop hdfs
   
![16](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/16.PNG)

**8. Создать для обоих дополнительных дисков разделы размером в 100% диска.**

For 1 disk :

   >sudo parted /dev/sdb
   
   >mklabel gpt
   
   >mkpart primary 0 5G
   
   >q
   
To cheng tipe for /dev/sdb:

   >fdisk /dev/sdb
   
   >t
   
   >20
   
   >w
   
For 2 disk:

   >sudo parted /dev/sdc
   
   >mklabel gpt
   
   >mkpart primary 0 5G
   
   >q
   
To cheng tipe for /dev/sdc:

   >fdisk /dev/sdc
   
   >t
   
   >20
   
   >w
   
**9. Инициализировать разделы из п.8 в качестве физических томов для LVM.**

   >sudo pvcreate /dev/sdb1
   
   >sudo pvcreate /dev/sdc1
   
For 8-9 tasks:

![17](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/17.PNG)

**10. Создать две группы LVM и добавить в каждую из них по одному физическому тому из п.9.**

   >sudo vgcreate LVM_sdb1 /dev/sdb1
   
   >sudo vgcreate LVM_sdc1 /dev/sdc1
   
Check:

   >sudo vgdisplay
   
![18](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/18.PNG)

**11. В каждой из групп из п.10 создать логический том LVM размером 100% группы.**

   >sudo lvcreate -l +100%FREE -n LVM_lvolumesdb LVM_sdb1
   
   >sudo lvcreate -l +100%FREE -n LVM_lvolumesdc LVM_sdc1
   
Check:

   >sudo lvdisplay
   
![19](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/19.PNG)

**12. На каждом логическом томе LVM создать файловую систему ext4.**

   >sudo mkfs.ext4 /dev/LVM_sdb1/LVM_lvolumesdb
   
   >sudo mkfs.ext4 /dev/LVM_sdc1/LVM_lvolumesdc
   
Check:

   >sudo blkid
   
![20](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/20.PNG)

**13. Создать директории и использовать их в качестве точек монтирования файловых систем из п.12: /opt/mount1; /opt/mount2**

To create directories:

   >sudo mkdir -p /opt/mount1
   
   >sudo mkdir -p /opt/mount2
   
To mount file systems:

   >sudo mount /dev/LVM_sdb1/LVM_lvolumesdb /opt/mount1
   
   >sudo mount /dev/LVM_sdc1/LVM_lvolumesdc /opt/mount2
   
Check:

   >df -h
   
![21](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/21.PNG)

**14. Настроить систему так, чтобы монтирование происходило автоматически при запуске системы. Произвести монтирование новых файловых систем.**

To add in file fstab:

   >sudo vi /etc/fstab

   ```
   UUID=2461665b-6ae0-4e84-9a3f-b0c84516ebe2   /opt/mount1   ext4   defaults   0 0
   UUID=a9e66f9b-697c-43f4-99f4-3b76133701eb   /opt/mount2   ext4   defaults   0 0
 
   ```
   
   >reboot
   
![22](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/22.PNG)

**Для VM1(шаги 15-16):**

**15. После монтирования создать 2 директории для хранения файлов Namenode сервиса HDFS:**

   >sudo mkdir -p /opt/mount1/namenode-dir
   
   >sudo mkdir -p /opt/mount2/namenode-dir
   
**16. Сделать пользователя hdfs и группу hadoop владельцами этих директорий.**

   >sudo chown -R hdfs:hadoop /opt/mount1
   
   >sudo chown -R hdfs:hadoop /opt/mount1
   
![23](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/23.PNG)

**Для VM2 (шаги 17-20):**

**17. После монтирования создать 2 директории для хранения файлов Datanode сервиса HDFS:**

   >sudo mkdir -p /opt/mount1/datanode-dir
   
   >sudo mkdir -p /opt/mount2/datanode-dir

**18. Сделать пользователя hdfs и группу hadoop владельцами директорий из п.17.**

   >sudo chown -R hdfs:hadoop /opt/mount1
   
   >sudo chown -R hdfs:hadoop /opt/mount2
   
![24](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/24.PNG)

**19. Создать дополнительные 4 директории для Nodemanager сервиса YARN:**

   >sudo mkdir -p /opt/mount1/nodemanager-local-dir
   
   >sudo mkdir -p /opt/mount1/nodemanager-log-dir
   
   >sudo mkdir -p /opt/mount2/nodemanager-local-dir
   
   >sudo mkdir -p /opt/mount2/nodemanager-log-dir

**20. Сделать пользователя yarn и группу hadoop владельцами директорий из п.19.**

   >sudo chown -R yarn:hadoop /opt/mount1/nodemanager-local-dir
   
   >sudo chown -R yarn:hadoop /opt/mount1/nodemanager-log-dir
   
![25](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/25.PNG)

   >sudo chown -R yarn:hadoop /opt/mount2/nodemanager-local-dir
   
   >sudo chown -R yarn:hadoop /opt/mount2/nodemanager-log-dir
   
![26](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/26.PNG)

**Для обеих машин:**

**21. Настроить доступ по SSH, используя ключи для пользователя hadoop.**

First, I logged as hadoop:

   >sudo passwd hadoop (to gaved password)
   
   >su - hadoop
   
Then I created a new SSH key-pair:

   >ssh-keygen
   
And then copy key:

   >ssh-copy-id -i .ssh/id_rsa 192.168.180.14
   
After thet gave key for VM2:

   >ssh -i ~/.ssh/id_rsa exam@192.168.180.14
   
Try to connect:

![3](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/3.PNG)

**22. Добавить VM1 и VM2 в /etc/hosts.**

Create file config:

   >vi ~/.ssh/config

   
   ```
   
   Host dev
    HostName 192.168.180.14
    User VM2
    Port 22
    IdentityFile /home/hadoop/.ssh/id_rsa
	
 ```
	

And we give right permissions:

   >sudo chmod 600 /home/exam/.ssh/config

Then adding information about our VM2 in /etc/hosts:

   >sudo vi /etc/hosts

   
   ```
   
   192.168.180.12   VM2 worker
   192.168.180.14   VM1 headnode
   
   ```

   
Check:

   >ssh VM2
   
![4](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/4.PNG)

**23. Скачать файлы по ссылкам в /usr/local/hadoop/current/etc/hadoop/{hadoop-env.sh,core-site.xml,hdfs-site.xml,yarn-site.xml}. При помощи sed заменить заглушки на необходимые значения:

1. hadoop-env.sh (https://gist.github.com/rdaadr/2f42f248f02aeda18105805493bb0e9b)
Необходимо определить переменные JAVA_HOME(путь до директории с OpenJDK8, установленную в п.3), HADOOP_HOME(необходимо указать путь к симлинку из п.6) и HADOOP_HEAPSIZE_MAX(укажите значение в 512M).

2. core-site.xml (https://gist.github.com/rdaadr/64b9abd1700e15f04147ea48bc72b3c7)
Необходимо указать имя хоста, на котором будет запущена HDFSNamenode(VM1).

3. hdfs-site.xml (https://gist.github.com/rdaadr/2bedf24fd2721bad276e416b57d63e38)
Необходимо указать директории namenode-dir, а также datanode-dir, каждый раз через запятую (например, /opt/mount1/namenode-dir,/opt/mount2/namenode-dir.

4. yarn-site.xml (https://gist.github.com/Stupnikov-NA/ba87c0072cd51aa85c9ee6334cc99158)
Необходимо подставить имя хоста, на котором будет развернут YARNResourceManager(VM1), а также пути до директорий nodemanager-local-dirи nodemanager-log-dir (если необходимо указать несколько директорий, то необходимо их разделить запятыми)**

1. The task wiht hadoop-env.sh:

   >sudo wget -P /usr/local/hadoop/current/etc/hadoop/ https://gist.githubusercontent.com/rdaadr/2f42f248f02aeda18105805493bb0e9b/raw/6303e424373b3459bcf3720b253c01373666fe7c/hadoop-env.sh
   
   >sudo sed -i -E 's/\"\%PATH_TO_OPENJDK8_INSTALLATION\%\"/\/usr\/lib\/jvm\/jre-openjdk/g;s/\"\%PATH_TO_HADOOP_INSTALLATION\"/\/usr\/local\/hadoop\/current\//g;s/\"\%HADOOP_HEAP_SIZE\%\"/512M/g' /usr/local/hadoop/current/etc/hadoop/hadoop-env.sh
   
2. The task with core-site.xml:

   >sudo wget -P /usr/local/hadoop/current/etc/hadoop/ https://gist.githubusercontent.com/rdaadr/64b9abd1700e15f04147ea48bc72b3c7/raw/2d416bf137cba81b107508153621ee548e2c877d/core-site.xml

   >sudo sed -i -E 's/\%HDFS_NAMENODE_HOSTNAME\%/headnode:8020/g' /usr/local/hadoop/current/etc/hadoop/core-site.xml
   
![27](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/27.PNG)
   
3. The task with hdfs-site.xml:

   >sudo wget -P /usr/local/hadoop/current/etc/hadoop/ https://gist.githubusercontent.com/rdaadr/2bedf24fd2721bad276e416b57d63e38/raw/640ee95adafa31a70869b54767104b826964af48/hdfs-site.xml
   
For VM1:

   >sudo sed -i -E 's/\%NAMENODE_DIRS\%/\/opt\/mount1\/namenode-dir,\/opt\/mount2\/namenode-dir/g;s/\ %DATANODE_DIRS\%/\/opt\/mount1\/datanode-dir,\/opt\/mount2\/datanode-dir/g' /usr/local/hadoop/current/etc/hadoop/hdfs-site.xml

![28](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/28.PNG)

4. The task with yarn-site.xml:

   >sudo wget -P /usr/local/hadoop/current/etc/hadoop/ https://gist.githubusercontent.com/Stupnikov-NA/ba87c0072cd51aa85c9ee6334cc99158/raw/bda0f760878d97213196d634be9b53a089e796ea/yarn-site.xml
 
   >sudo sed -i -E 's/\%YARN_RESOURCE_MANAGER_HOSTNAME\%/headnode/g;s/\%\NODE_MANAGER_LOCAL_DIR\%/\/opt\/mount1\/nodemanager-local-dir,\/opt\/mount2\/nodemanager-local-dir/g;s/\%\NODE_MANAGER_LOG_DIR\%/\/opt\/mount1\/nodemanager-log-dir,\/opt\/mount2\/nodemanager-log-dir/g' /usr/local/hadoop/current/etc/hadoop/yarn-site.xml

![33](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/33.PNG)

**24. Задать переменную окружения HADOOP_HOME через /etc/profile**

   >sudo vi /etc/profile
   
   ```
	
	export HADOOP_HOME=/usr/local/hadoop/current
	
   ```
	
**Для VM1(шаги 25-26):**

**25. Произвести форматирование HDFS (от имени пользователя hdfs):**

Check our working of the changes of file /etc/profile:

   >ls -l $HADOOP_HOME/bin/hdfs
   
![5](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/5.PNG)
   
   >$HADOOP_HOME/bin/hdfs namenode -format cluster1
   
![6](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/6.PNG)

**26. Запустить демоны сервисов Hadoop:**

Running from user hdfs:

   >$HADOOP_HOME/bin/hdfs --daemon start namenode
   
It is runing:
   
![7](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/7.PNG)

Running from user yarn:

   >$HADOOP_HOME/bin/yarn --daemon start resourcemanager
   
**Для VM2(шаг 27):**

**27. Запустить демоны сервисов:**

Running fron user hdfs:

   >$HADOOP_HOME/bin/hdfs --daemon start datanode
   
Running fron user yarn:

   >$HADOOP_HOME/bin/yarn --daemon start nodemanager

**28. Проверить доступность Web-интефейсов HDFS Namenode и YARN Resource Manager по портам 9870 и 8088 соответственно (VM1).<< порты должны быть доступны с хостовой системы.**

The add all ports:

   >sudo firewall-cmd --zone=public --add-port=8088/tcp --permanent
   
   >sudo firewall-cmd --zone=public --add-port=9870/tcp --permanent
   
Making the enp0s8 interface public:

   >sudo firewall-cmd --permanent --zone=public --add-interface=enp0s8
   
Reload firewall:

   >sudo firewall-cmd --reload
   
Check:
 
In browser we writing:

   >192.168.180.12:9870
   
   >192.168.180.12:8088
   
![31](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/31.PNG)

![32](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/34.PNG)
   
**29. Настроить управление запуском каждого компонента Hadoop при помощи systemd(используя юниты-сервисы).**

**For VM1.**

29.1. The configure launch control for user hdfs.
   
To create daemon service in /etc/systemd/system/multi-user.target.wants:

   >sudo vi /etc/systemd/system/multi-user.target.wants/my_daemon_hadoop_hdfs.service
   

   ```
   
   [Unit]
   Description=Deamon 1 for hdfs

   [Service]
   Type=forking
   User=hdfs
   Group=hadoop
   ExecStart=/usr/local/hadoop/current/bin/hdfs --daemon start namenode
   ExecStop=/usr/local/hadoop/current/bin/hdfs --daemon stop namenode
   
   ```
   

Now to reload system:

   >systemctl daemon-reload
   
The add the created unit to autoload:

   >systemctl enable my_daemon_hadoop_hdfs.service

29.2. The configure launch control for user yarn.
   
To create daemon service in /etc/systemd/system/multi-user.target.wants:

   >sudo vi /etc/systemd/system/multi-user.target.wants/my_daemon_hadoop_yarn.service
   
   ```
   
   [Unit]
   Description=Deamon 2 for yarn

   [Service]
   Type=forking
   User=yarn
   Group=hadoop
   ExecStart=/usr/local/hadoop/current/bin/yarn --daemon start resourcemanager
   ExecStop=/usr/local/hadoop/current/bin/yarn --daemon stop resourcemanager
   
   ```
   
Now to reload system:

   >systemctl daemon-reload
   
The add the created unit to autoload:

   >systemctl enable my_daemon_hadoop_yarn.service
   
![29](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/29.PNG)
   
**For VM2.**

29.3. The configure launch control for user hdfs.
   
To create daemon service in /etc/systemd/system/multi-user.target.wants:

   >sudo vi /etc/systemd/system/multi-user.target.wants/my_daemon_hadoop_hdfs.service
   
   ```
   
   [Unit]
   Description=Deamon 3 for hdfs

   [Service]
   Type=forking
   User=hdfs
   Group=hadoop
   ExecStart=/usr/local/hadoop/current/bin/hdfs --daemon start datanode
   ExecStop=/usr/local/hadoop/current/bin/hdfs --daemon stop datanode
   
   ```
   
Now to reload system:

   >systemctl daemon-reload
   
The add the created unit to autoload:

   >systemctl enable my_daemon_hadoop_hdfs.service
   
29.4. The configure launch control for user yarn.
   
To create daemon service in /etc/systemd/system:

   >sudo vi /etc/systemd/system/multi-user.target.wants/my_daemon_hadoop_yarn.service
   
   ```
   
   [Unit]
   Description=Deamon 4 for yarn

   [Service]
   Type=forking
   User=yarn
   Group=hadoop
   ExecStart=/usr/local/hadoop/current/bin/yarn --daemon start nodemanager
   execStop=/usr/local/hadoop/current/bin/yarn --daemon stop nodemanager
   
   ```
   
Now to reload system:

   >systemctl daemon-reload
   
The add the created unit to autoload:

   >systemctl enable my_daemon_hadoop_yarn.service
   
![30](https://gitlab.com/radosdevopsstudent/training/-/raw/master/Exam%20%E2%84%961/30.PNG)
