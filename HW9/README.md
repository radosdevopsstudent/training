**Homework №9 from 09.02.21**

# The task 1.
 
**Imagine you was asked to add new partition to your host for backup purposes. To simulate appearance of new physical disk in your server, please create new disk in Virtual Box (5 GB) and attach it to your virtual machine.
Also imagine your system started experiencing RAM leak in one of the applications, thus while developers try to debug and fix it, you need to mitigate OutOfMemory errors; you will do it by adding some swap space.
/dev/sdc - 5GB disk, that you just attached to the VM (in your case it may appear as /dev/sdb, /dev/sdc or other, it doesn't matter)**

**1.0. We create a 5G hard disk in our Virtual Machine. Let's take a look at the machine.**

There is :)

![1](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW9/1.PNG)

**1.1. Create a 2GB   !!! GPT !!!   partition on /dev/sdb of type "Linux filesystem" (means all the following partitions created in the following steps on /dev/sdc will be GPT as well).**

 Open our disk:
 
   >parted /dev/sdb
 
 To create GPT label:
 
   >mklabel gpt
 
 To create a new partition and out from parted:
 
   >mkpart primary 0 2G
 
   >q
 
 Now to enter into fdisk:
 
   >fdisk /dev/sdb
 
 To change file system on Linux system:
 
   >t
 
   >20
 
   >w
 
![2](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW9/2.PNG)

**1.2. Create a 512MB partition on /dev/sdb of type "Linux swap".**

 Start from command fdisk:
 
   >fdisk /dev/sdb
   
 To add a new partition:
 
   >n
   
 Choosing number partition:
 
   >2
   
 Choosing 1 and 2 sectors:
 
   >4196352
   
   >+512M

 Choosing type "Linux swap" and out:
 
   >19
   
   >w
   
 Running command for the kernel to see the newly created swap partition:
 
   >partprobe
   
![3](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW9/3.PNG)
  
**1.3. Format the 2GB partition with an XFS file system.**

   >mkfs.xfs /dev/sdb1
   
**1.4. Initialize 512MB partition as swap space.**

 Initialize 512MB partition as swap space:
 
   >mkswap /dev/sdb2 
   
 Add in file /etc/fstab:
 
   ```
   UUID=521d42d5-7417-4f74-a483-438962d69866       swap    swap    defaults    0 0
   
   ```
   
 And running our swap:
 
   >swapon -a
   
   >swapon -s
   
![4](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW9/4.PNG)

**1.5. Configure the newly created XFS file system to persistently mount at /backup.**
 
 Do mount:
 
   >mount -a /dev/sdb1
   
 Editing the file /etc/fstab:
 
   >vi /etc/fstab
   
   ```
   
   /dev/sdb1       /backup      xfs       defaults    1 2
   
   ```
   
**1.6. Configure the newly created swap space to be enabled at boot.**

 It was done in paragraph 1.4. I wrote this in file /etc/fstab.
 
**1.7. Reboot your host and verify that /dev/sdc1 is mounted at /backup and that your swap partition  (/dev/sdc2) is enabled.**

   >reboot
   
   >mount | grep /dev/sdb1
   
![5](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW9/5.PNG)

   >swapon -s
   
![6](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW9/6.PNG)

# The task 2.

**LVM. Imagine you're running out of space on your root device. As we found out during the lesson default CentOS installation should already have LVM, means you can easily extend size of your root device. So what are you waiting for? Just do it!**
 
**2.1. Create 2GB partition on /dev/sdc of type "Linux LVM"**

 Start:
 
   >fdisk /dev/sdb
   
 Partition number:
    
	>3
	
 First sector (default):
 
   >5244928 
   
 Last sector:
 
   >+2G
   
 Partition type:
 
   >31
   
   >w
   
   >partprobe
   
![7](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW9/7.PNG)

**2.2. Initialize the partition as a physical volume (PV)**

   >pvcreate /dev/sdb3
   
 Chek:
 
   >pvdisplay
   
![8](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW9/8.PNG)

**2.3. Extend the volume group (VG) of your root device using your newly created PV**

   >vgextend centos /dev/sdb3
   
 Chek:
 
   >vgdisplay
   
![9](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW9/9__2_.PNG)

**2.4. Extend your root logical volume (LV) by 1GB, leaving other 1GB unassigned.**
 
   >lvextend -L +1G /dev/centos/root
   
 Chek:
 
   >lvdisplay
   
![10](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW9/10.PNG)

**2.5. Check current disk space usage of your root device**

   >df -h /dev/centos/root
   
![11](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW9/11.PNG)

**2.6. Extend your root device filesystem to be able to use additional free space of root LV.**

   >xfs_growfs /dev/mapper/centos-root
   
![12](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW9/12.PNG)

**2.7. Verify that after reboot your root device is still 1GB bigger than at 2.5.**

![13](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW9/13.PNG)
