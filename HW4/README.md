**Homework №4 from 21.01.21**

# The task 1. Processes

1. Run a sleep command three times at different intervals. 

 I executed the command "sleep" 3 times at intervals of 200, 300 and 400 seconds.

![1](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW4/1.PNG)

2. Send a SIGSTOP signal to all of them in three different ways.

 For this task I started the process "sleep 1000". For SIGSTOP I used next commands:
 
   >kill -19 2604
   
   >pkill -19 sleep
   
   >kill -stop 2631
   
3. Check their statuses with a job command.

![2](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW4/2.PNG)

4. Terminate one of them. (Any)

   >kill -15 2635.
 
![3](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW4/3.PNG)

5. To other send a SIGCONT in two different ways.

   >kill -18 2604
   
   >kill -SIGCONT 2621
   
6. Kill one by PID and the second one by job ID.

 6.1. For killing by ID:

   >kill -9 2648
   
 6.2. For killing by PID:
 
   >kill -9 %11
   
# The task 2. systemd

1. Write two daemons: one should be a simple daemon and do sleep 10 after a start and then do echo 1 > /tmp/homework, the second one should be one shot and doecho 2 > /tmp/homework with out any sleep.
 
 **TO CREATE FIRST DAEMON**
 
 1.1. First, I crete unit file with daemon in directory /etc/systemd/system:
   
   >sudo vi my_daemon_1.service
   
 In unit file wrote:
   ```
   [Unit]
   Description=Deamon 1 wiht echo 10

   [Service]
   Type=simple
   Restart=always
   RestartSec=10
   User=radosteva
   ExecStart=/home/radosteva/daemon1.sh

   ```
   
 1.2. Then, I created next file daemon1.sh in directory /home/radosteva:
   
   >touch daemon1.sh
   
   >vi daemon1.sh
   
   ```
    #!/bin/bash

    sleep 10
    echo 1 >> /tmp/homework
   ```
   
 1.3. Next, reload systemd:
 
   >systemctl daemon-reload
   
 1.4. Add the created unit to autoload:
 
   >systemctl enable my_daemon_1.service
   
 1.5. Let's run:
 
   >systemctl start my_daemon_1
 
![8](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW4/8.PNG)
 
 It's working :)
 
**TO CREATE SECOND DAEMON**

 1.1. To crete unit file with daemon in directory /etc/systemd/system:
   
   >sudo vi my_daemon_2.service
   
 In unit file wrote:
   ```
   [Unit]
   Description=Deamon 2

   [Service]
   Type=oneshot
   User=radosteva
   ExecStart=/home/radosteva/daemon2.sh
   ```
   
 1.2. Then, I created next file daemon2.sh in directory /home/radosteva:
   
   >vi daemon2.sh
   
   ```
   #!/bin/bash

   echo 2 >> /tmp/homework

   ```
   
 1.3. Next, reload systemd:
 
   >systemctl daemon-reload
   
 1.4. Add the created unit to autoload:
 
   >systemctl enable my_daemon_2.service
   
 1.5. Let's run:
 
   >systemctl start my_daemon_2
 
![9](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW4/9.PNG)
  
It's working :)
  
2. Make the second depended on the first one (should start only after the first).

 For deamon my_daemon_2.service we will write "After":
 
   ```
   [Unit]
   Description=Deamon 2

   [Service]
   After=my_daemon_1.service
   Type=oneshot
   User=radosteva
   ExecStart=/home/radosteva/daemon2.sh

   ```
   
3. Write a timer for the second one and configure it to run on 01.01.2019 at 00:00.

 3.1. To create unit file daemo2time.timer in directory /etc/systemd/system:
 
   >sudo touch daemo2time.timer
   
 And write next:
 
   ```
   [Unit]
   Description=Timer for my_daemon_2.service

   [Timer]
   OnCalendar=2019-01-01 00:00
   Unit=my_daemon_2.service

   [Install]
   WantedBy=timers.target

   ```
   
**To make sure that the script works, I changed the date with the current one and let it run. Everything works, which means that as of 01.01.2019 it would also work.**

![10](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW4/10.PNG)
 
4. Start all daemons and timer, check their statuses, timer list and /tmp/homework.

 4.1. Runing my_daemon_1.service
 
   >systemctl start my_daemon_1.service
   
   >systemctl status -l my_daemon_1.service
   
![11](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW4/11.PNG)

 4.2. Runing my_daemon_2.service
 
   >systemctl start my_daemon_2.service
   
   >systemctl status -l my_daemon_2.service
   
![12](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW4/12.PNG)

 4.3. Runing daemo2time.timer
 
   >systemctl start daemo2time.timer
   
   >systemctl status -l daemo2time.timer
   
![13](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW4/13.PNG)
 
5. Stop all daemons and timer.

   >systemctl stop daemo2time.timer

   >systemctl stop my_daemon_1.service

   >systemctl stop my_daemon_2.service
   
# The task 3. Systemd

1. Create an anacron job which executes a script withecho Hello > /opt/hello and runs every 2 days.

1.1. First I wrote script on sh:
 
   ```
   echo Hello > /opt/hello  
   
   ```
   
 1.2. Then I created file myanacrontab.service in directory /etc/:
 
   ```
   2       15      test.daily      /bin/sh /home/radosteva/anacron.sh
   
   ```

 1.2. After that I gave permisison for file with my script:
 
   >chmod +x myanacrontab.service
	
 1.3. Then I restart anacron:
 
   >sudo anacron -fdn

2. Create a cron job which executes the same command (will be better to create a script for this) and runs it in 1 minute after system boot.

 2.1. First I wrote script. Used command crontab -e:

   ```
   reboot sleep 60 && 0 0 */2 * * echo Hello > /opt/hello
   ```
   
 2.2. After that I gave all permisison for file with my script:
 
   >chmod ug+x crontab.ebNlHj
   
 2.3. Then I restart anacron:
 
   >sudo anacron -fdn
   
![5](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW4/5.PNG)

# The task 4. lsof

1. Run a sleep command, redirect stdout and stderr into two different files (both of them will be empty).

   >sleep 20 2>> error_file 1>> output_file
   
2. Find with the lsof command which files this process uses, also find where it gets stdout from.

 2.1. First, I started process sleep:

   >sleep 200 2>> error_file 1>> output_file &
   
 2.2. Then with № our process I wached all needs infomation: 

   >lsof -p 1875
   
![6](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW4/6.PNG)

 **Process use libs:**

 /usr/bin/sleep

 /usr/lib/locale/locale-archive

 /usr/lib64/libc-2.17.so

 /usr/lib64/ld-2.17.so

 **Process use files:**

 /home/radosteva/output_file

 /home/radosteva/error_file

 **stdout from:**
 
 /home/radosteva/output_file

3. List all ESTABLISHED TCP connections ONLY with lsof.

   >lsof -i -sTCP:ESTABLISHED
   
![7](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW4/7.PNG)
