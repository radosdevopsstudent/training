**Homework from 26.01.21**

# The task 1.
 
1.1. SSH to remotehost using username and password provided to you in Slack. Logout from remotehost.
 
   >ssh Anastasiia_Radosteva@40.68.74.188
   
   >exit
   
![1](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW5/1.PNG)

1.2. Generate new SSH key-pair on your localhost with name "hw-5" (keys should becreated in ~/.ssh folder).

   >ssh-keygen -t rsa -C Anastasiia_Radosteva@40.68.74.188 -f ~/.ssh/hw_5 (AnxTODZRh8AhNhNqKkpMS20Z5FmpH1O+2mqyNGATYjA)

![2](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW5/2.PNG)

1.3. Set up key-based authentication, so that you can SSH to  remotehost  without password.

   >ssh-copy-id -i ~/.ssh/hw_5 Anastasiia_Radosteva@40.68.74.188
   
![3](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW5/3.PNG)

1.4. SSH to remotehost without password. Log out from remotehost.

   >ssh -i ~/.ssh/hw-5 Anastasiia_Radosteva@40.68.74.188
   
1.5. Create SSH config file, so that you can SSH to remotehost simply running`sshremotehost` command. As a result, provide output of command`cat ~/.ssh/config`.

   >vi ~/.ssh/config
   
   >cmod 600 /home/radosteva/.ssh/config
   
   >cat /home/radosteva/.ssh/config
   
   ```
   Host dev
    HostName 40.68.74.188
    User Anastasiia_Radosteva
    Port 22
    IdentityFile /home/radosteva/.ssh/hw-5
	
   ```
   
1.6. Using command line utility (curl or telnet) verify that there are some webserverrunning on port 80 of webserver.  Notice that webserver has a private network IP, so you can access it only from the same network (when you are on remotehost that runsin the same private network). Log out from remotehost.
  
 We went on remotehost and after that wrote next:
 
   >curl 10.0.0.5:80
   
![4](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW5/4.PNG)

1.7. Using SSH setup port forwarding, so that you can reach  webserver from your local host (choose any free local port you like).

   >

   

   
# The task 2.

2.1. Imagine your localhost has been relocated to Havana. Change the time zone onthe localhost to Havana and verify the time zone has been changed properly (may bemultiple commands).

 First, I cheked all available time zones in the system:
 
   >timedatectl list-timezones
   
 Then I chenge my timezone:
 
   >timedatectl set-timezone America/Havana
   
   Check :)
   
![5](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW5/5.PNG)

2.2. Find all systemd journal messages on localhost, that were recorded in the last 50minutes and originate from a system service started with user id 81 (single command).

   >journalctl --since "50 min ago" _UID=81
   
![6](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW5/6.PNG)

2.3. Configure rsyslogd by adding a rule to the newly created configuration file/etc/rsyslog.d/auth-errors.conf to log all security and authentication messages with thepriority alert and higher to the /var/log/auth-errors file. Test the newly added logdirective with the logger command (multiple commands).
   
   >vi /etc/rsyslog.d/auth-errors.conf
   
   ```
   
   #content of /etc/rsyslog.d/auth-errors.conf
   auth,authpriv.alert    /var/log/auth-errors
   
   ```
   
   >sudo systemctl restart rsyslog
   
   >logger -p auth.alert "This is an alert message"
   
   >cat /var/log/auth-errors
   
![7](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW5/7.PNG)
