Homework №1 from 12.01.21
***
# The task 1.
   1. For search all files with the word “config” at names in catalog /usr/share/man/manX, in the terminal we use command:
	   >ls /usr/share/man/man* | grep config
   2. For search all files with word “system” at names in catalog usr/share/man/man1, in the terminal we use command:
	   >ls /usr/share/man/man1 | grep system
   3. For search all files with word “system” at names in catalog /usr/share/man/man7, in the terminal we are use command:
	   >ls /usr/share/man/man7 | grep system
   4. **Required version for 1 and 2 tasks**:
      >ls /usr/share/man/man*/*config*

      >ls /usr/share/man/man{1,7}/*system* 
***
# The task 2.
   1. Search files with word "help" at name in catalog /usr/share/man using command find:
      >find /usr/share/man -iname '*help*'

      1.2. **Required version for 1 task**:
      >find /usr/share/man -iname *help*

   2. Search for files with the word "conf" at the beginning of the name in the directory usr/share/man:
	   >find /usr/share/man -iname 'conf*'
   3. With the "find" command we can delete files of a certain format. Exemple: 
	   >find . -type f -name "file*.txt" -exec rm -f {} \;
***
# The task 3.
   1. Output 2 latest terms of file /etc/fstab:
      >tail -n 2 /etc/fstab
   2. Output 7 first terms of the file /etc/fstab:
	  >head –n 7 /etc/yum.conf
   3. When we try to request more lines in a file /etc/fstab using a command "tail", the command shows us all the possible contents of the file. 
   4. When we try to request more lines in a file /etc/yum.conf using a command "head", the command shows us all the possible contents of the file.
***
# The task 4.
   1. To create files 1,2,3:
      >touch file_name{1..3}.md
   2. Rename files file(1-3).md
      >mv file_name1.{md,texdoc}

      >mv file_name2{.md,}

      >mv file_name3.md{,.latest}

      >mv file_name1.{texdoc,txt}
***
# The task 5.

  1. cd 

  2. cd - 

  3. cd ~

  4. cd /home/radosteva

  5. cd ../home/radosteva
***
# The task 6.

1. To create 3 directory new,processed,in-process. In directory "in-process" we create 3 directory tread(0-2):

      >mkdir -p ~/{new,processed,in-process/{tread0,tread1,tread2}}

     2. In directory "new" create files data(0-99):
      >touch data{0..99}.txt

     3. Copy 34 files from directory "new" in directory "tread0":

      >cp ~/new/data{0..33}.txt ~/in-process/tread0/

      4. Copy 33 files from directory "new" in directory "tread1":

      >cp ~/new/data{34..66}.txt ~/in-process/tread1/

      5. Copy 33 files from directory "new" in directory "tread2":

      >cp ~/new/data{67..99}.txt ~/in-process/tread2/

      6. Let's display the contents of the in-process directory:

      >ls ~/in-process/tread*/data{0..99}.txt

      7. Transfer all files from all directories "treead(0-2)" to the directory "processed":
      >mv ~/in-process/tread*/* ~/processed/

      8. Let's display the contents of the in-process and processed directories:

      >ls ~/in-process ~/processed

      9. We compare the number of files in the "new" and "processed" directories and if they are equal, then delete contents in directory "new":

	  >if [[ $(ls ~/new/ -f  | wc -l) -eq $(ls ~/processed/ -f  | wc -l) ]]

      > then

      > rm -f ~/new/*

      >fi

   9.1. **Required version for 9 task**:
      >if [[ $(ls ~/new/ -f  | wc -l) -eq $(ls ~/processed/ -f  | wc -l) ]] ; then rm -f ~/new/* ; fi 
***
# The task 7.
   1. To expand curly braces we need to change string:

       >echo file{$a..$b} 
       
        to 

       >eval "echo file{$a..$b}"
***
# The task 0. radosteva - VM1. test - VM2.
   1. First, I installed iptables on second machine.
   ![1](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW1/1.png)

   2. Then I gave permission to the first machine to receive and send packets from the first machine.
   ![2](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW1/2.png)
   
   3. After that I denied access and exit to everything and everyone.
   ![3](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW1/3.PNG)

   4. As we can see only 1 machin has access to the second test machine.

   ![4](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW1/4.PNG)

   5. After all the actions done, we see that we can connect from 1 to 2 machine via ssh.
   ![5](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW1/5.PNG)

   6. First machine have connect. Second machine don't have connect with outside world.
   ![6](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW1/6.PNG)
   ![7](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW1/7.PNG)

   7. And last, we check our ping connect between 1 and 2 machine.

   ![8](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW1/8.PNG)
   
   ![9](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW1/9.PNG)

   The end :)
