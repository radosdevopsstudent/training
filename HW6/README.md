**Homework №6 from 28.01.21**

# The task 1.

1. С помощью утилиты nmcli добавте второй ip адрес сетевому интерфейсу enp0s3 (или тому, который является "основным" для вашей машины)
   
   >sudo nmcli conn mod "enp0s8" +ipv4.addresses 172.31.110.25/30
   
   >reboot
   
![1](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW6/1.PNG)
 
 And finally checked ssh connection:
 
![2](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW6/2.PNG)

# The task 2.

Новый IP адрес должен "резолвиться" в "private" DNS запись, а hostname вашей машины должен быть таким же, как у ближайшей галактики к нашей Солнечной системе (ну или выберете обычное скучное имя). Продемонстрируйте результаты с помощью  одной из утилит (dig, nslookup, host)* или другой утиилитой.
 
 First, I add my ip in file /etc/hosts
   
   >172.31.110.26 satana.rad.net
   
 After that I add my name in file /etc/hostname

    >satana.rad.net
   
 Check :) Here I want to note that the name for ip has nothing to do with the teacher. 
 
![3](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW6/3.PNG)
   
# The task 3.

3.1 Подключаетесь по ssh ко второму интерфейсу машины, логинетесь.

 Changing the interface for 1 page of putty and open 2 session from another ip on putty:
 
   >ssh 10.0.2.15

3.2. В одной сессии запускаете tcpdum, в другой сессии пытаетесь получить используя любой http клиент контент страницы по адресу: example.com.
3.2* Получите контент страницы с помощтью telnet.
 
 In session wiht tcpdamp we write newt:
 
   >sudo tcpdump -e -i enp0s3 -v -A host example.com
   
 In session with telnet we input next:
 
   >telnet example.com 80
   
 And we get next:
 
(https://gitlab.com/radosdevopsstudent/training/-/blob/master/HW6/need.txt) file!

# The task 4.

   >sudo nmap --top-ports 10 79.134.223.227
   
![5](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW6/5.PNG)
   
 To found all open ports I had to check all ports and only 1 port was open.
 
   >sudo nmap -p 60000-61000 79.134.223.227
   
![6](https://gitlab.com/radosdevopsstudent/training/-/raw/master/HW6/6.PNG)
